import { useState, useEffect } from "react";
import { Carousel, Button, ListGroupItem } from "react-bootstrap";
import { Link } from "react-router-dom"; 


const AppCarouselPage = () => {
    const carouselItems = [
        {
            id: 1,
            img: {
                src: "https://images.pexels.com/photos/667986/pexels-photo-667986.jpeg?cs=srgb&dl=pexels-edward-eyer-667986.jpg&fm=jpg",
                alt: "First Slide"
            },
            captions: {
                title: "NEED A DRINK?",
                text: "We provide quality booze here."
            }
        },
        {
            id: 2,
            img: {
                src: "https://images.pexels.com/photos/2796105/pexels-photo-2796105.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
                alt: "Second Slide"
            },
            captions: {
                title: "YOU DESERVE THIS",
                text: "Shop now for reasonable prices."
            }
        },
        {
            id: 3,
            img: {
                src: "https://images.pexels.com/photos/51365/drink-alcohol-cup-whiskey-51365.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
                alt: "Third Slide"
            },
            captions: {
                title: "BUY NOW, LET'S GET DRUNK!",
                text: "Don't worry, we got you here."
            }
        }
    ];

    return (
        <Carousel>
            {
                carouselItems.map(item => {
                    const {id, img, captions} = item;
                    return(
                        <Carousel.Item className="frontPageItem" key={id}>
                            <img 
                                className="d-block w-100"
                                src={img.src} 
                                alt={img.alt}
                            />
                            <Carousel.Caption>
                                <h1 className="mb-5">{captions.title}</h1>
                                <h4 className="mb-5">{captions.text}</h4>
                                <Button className="ctaBtn mb-5" as={Link} to="/shop">SHOP NOW</Button>
                            </Carousel.Caption>
                        </Carousel.Item>
                    )
                })
            }
        </Carousel>
    )
}

export default AppCarouselPage;
